from mongotest import mongotest
from pymongo import MongoClient

def test_mongotest() -> None:
    client = MongoClient('mongo', 27021)
    assert mongotest(client)
