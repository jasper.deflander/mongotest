from pymongo import MongoClient

def mongotest(client: MongoClient) -> bool:
    collection = client.database.collection
    document = {}
    collection.insert_one(document)
    return collection.find_one() is not None
